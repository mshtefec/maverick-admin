export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBnTR9W7c5pkMcR38yq3pgIAo45YiN-2B0',
    authDomain: 'maverickrender-dev.firebaseapp.com',
    databaseURL: 'https://maverickrender-dev.firebaseio.com',
    projectId: 'maverickrender-dev',
    storageBucket: 'maverickrender-dev.appspot.com',
    messagingSenderId: '482595391053'
  },
  version: '0.0.1'
};

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  // PRODUCTION
  // firebase: {
  //   apiKey: 'AIzaSyBnTR9W7c5pkMcR38yq3pgIAo45YiN-2B0',
  //   authDomain: 'maverickrender-dev.firebaseapp.com',
  //   databaseURL: 'https://maverickrender-dev.firebaseio.com',
  //   projectId: 'maverickrender-dev',
  //   storageBucket: 'maverickrender-dev.appspot.com',
  //   messagingSenderId: '482595391053'
  // },
  // DEV
  firebase: {
    apiKey: "AIzaSyDFmWgzNmOXofvNcxEBfMuSIw8vXIuuhV8",
    authDomain: "maverick-angular.firebaseapp.com",
    databaseURL: "https://maverick-angular.firebaseio.com",
    projectId: "maverick-angular",
    storageBucket: "maverick-angular.appspot.com",
    messagingSenderId: "91123531669"
  },
  version: '0.0.1'
};



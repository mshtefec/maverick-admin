import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';

interface Image {
  id?: string;
  author: string;
  category: string;
  order: number;
}

@Component({
  selector: 'app-image-update',
  templateUrl: './image-update.component.html',
  styleUrls: ['./image-update.component.scss']
})
export class ImageUpdateComponent implements OnInit {
  
  imageId;

  image: Image;
  publicURL: Observable<string>;

  categories = [
    { 'name' : 'VFX'},
    { 'name' : 'CAD'},
    { 'name' : 'OTHER'},
  ]

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private angularFirestore: AngularFirestore,
    private storage: AngularFireStorage
  ) {}

  ngOnInit() {
    this.imageId = this.route.snapshot.paramMap.get('id');
    this.angularFirestore.doc('gallery/' + this.imageId).get().toPromise().then(
      (snapshot) => {
        this.image = { 
          author: snapshot.data().author, 
          category: snapshot.data().category,
          order: snapshot.data().order
        };
        this.publicURL = this.storage.ref('gallery/' + snapshot.data().name).getDownloadURL();
      }
    )
  }

  onSubmit(imageForm: NgForm) {
    this.updateFile(imageForm.value);
  }

  updateFile(imageModel: Image){

    this.angularFirestore.collection('gallery').doc(imageModel.id).update(
      { author: imageModel.author, category: imageModel.category, order: imageModel.order}
    )

    this.router.navigate(['images']);
    // this.angularFirestore.doc('gallery/' + this.imageId).set(
    //   { author: image.author, category: image.category, order: image.order}
    // );

  }
  
}

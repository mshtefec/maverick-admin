import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import * as firebase from 'firebase';

interface Image {
  id?: string;
  name: string;
  author: string;
  category: string;
  order: number;
}

@Component({
  selector: 'app-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.scss']
})
export class ImageListComponent implements OnInit {

  //imagesCollection: AngularFirestoreCollection<any>;
  //images: Observable<any[]>;
  imagesList: Image[] = [];
  
  columns = [
    { 'name' : 'name'},
    { 'name' : 'author'},
    { 'name' : 'category'},
    { 'name' : 'order'},
    { 'name' : 'actions'},
  ]

  constructor(
    public router: Router,
    private angularFirestore: AngularFirestore,
    private angularFireStorage: AngularFireStorage
  ) {}

  ngOnInit() {
    //this.imagesCollection = this.angularFirestore.collection('gallery')
    //this.images = this.imagesCollection.valueChanges()

    this.angularFirestore.collection('gallery').get().toPromise().then(
      (snapshot) => {
        snapshot.docs.forEach(doc => {
          let imagesModel: Image = { 
            id: doc.id,
            name: doc.data().name,
            author: doc.data().author,
            category: doc.data().category,
            order: doc.data().order
          };
          this.imagesList.push(imagesModel);
        })
      }
    )

  }

  onShow(id: string) {
    this.router.navigate(['images/show', id]);
  }  

  onEdit(id: string) {
    this.router.navigate(['images/edit', id]);
  } 
  
  deleted(image: Image) {

    const imageRefStore = this.angularFirestore.collection('gallery');
    const imageRefStorage = firebase.storage().ref().child(`gallery/${image.name}`);

    imageRefStore.doc(image.id).delete().then(function() {
      console.log("Store successfully deleted!");
    }).catch(function(error) {
      console.error("Error removing document: ", error);
    });

    // Delete the file
    imageRefStorage.delete().then(function() {
      console.log("Storage successfully deleted!");
    }).catch(function(error) {
      console.error("Error removing document: ", error);
    });

    location.reload();
  } 
  
}

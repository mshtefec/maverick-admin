import { Component, OnInit } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { switchMap, filter, tap } from 'rxjs/operators';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-image-create',
  templateUrl: './image-create.component.html',
  styleUrls: ['./image-create.component.scss']
})
export class ImageCreateComponent implements OnInit {

  // Main task 
  task: AngularFireUploadTask;

  // Progress monitoring
  percentage: Observable<number>;

  snapshot: Observable<any>;

  // Download URL
  downloadURL: Observable<string>;

  // State for dropzone CSS toggling
  isHovering: boolean;

  categoryDefault = 'OTHER';
  categories = [
    { 'name' : 'VFX'},
    { 'name' : 'CAD'},
    { 'name' : 'OTHER'},
  ]

  constructor(
    private storage: AngularFireStorage, 
    private db: AngularFirestore
  ) {}

  ngOnInit() {}

  toggleHover(event: boolean) {
    this.isHovering = event;
  }


  startUpload(event: FileList, uploadForm: NgForm) {
    // The File object
    const file = event.item(0)

    // Client-side validation example
    if (file.type.split('/')[0] !== 'image') { 
      console.error('unsupported file type :( ')
      return;
    }

    const time = new Date().getTime();

    // The storage path
    const path = `gallery/${time}_${file.name}`;

    // Totally optional metadata
    const customMetadata = { 
      app: 'Maverick',
      author: uploadForm.value.author || null,
      category: uploadForm.value.category || null,
      order: uploadForm.value.order || 0
    };

    // The main task
    this.task = this.storage.upload(path, file, { customMetadata })

    // Progress monitoring
    this.percentage = this.task.percentageChanges();
    this.snapshot   = this.task.snapshotChanges().pipe(
      tap(snap => {
        if (snap.bytesTransferred === snap.totalBytes) {
          // Update firestore on completion
          this.db.collection('gallery').add( 
            { 
              path, 
              name: `${time}_${file.name}`,
              size: snap.totalBytes,
              author: uploadForm.value.author || null,
              category: uploadForm.value.category || null,
              order: uploadForm.value.order || 0
            }
          )
        }
      })
    )

    const ref = this.storage.ref(path);

    // The file's download URL
    this.downloadURL = this.task.snapshotChanges().pipe(
      filter(snap => snap.state === 'SUCCESS'),
      switchMap(() => ref.getDownloadURL())
    )
    
  }

  // Determines if the upload task is active
  isActive(snapshot) {
    return snapshot.state === 'running' && snapshot.bytesTransferred < snapshot.totalBytes
  }
}

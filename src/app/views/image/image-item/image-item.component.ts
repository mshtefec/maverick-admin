import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';

interface Image {
  id?: string;
  name: string;
  author: string;
  category: string;
  order: number;
}

@Component({
  selector: 'app-image-item',
  templateUrl: './image-item.component.html',
  styleUrls: ['./image-item.component.scss']
})
export class ImageItemComponent implements OnInit {
  
  imageId;
  
  image: Image;
  publicURL: Observable<string>;

  constructor(
    private route: ActivatedRoute,
    private angularFirestore: AngularFirestore,
    private storage: AngularFireStorage
  ) {}

  ngOnInit() {
    this.imageId = this.route.snapshot.paramMap.get('id');
    this.angularFirestore.doc('gallery/' + this.imageId).get().toPromise().then(
      (snapshot) => {
        this.image = { 
          name: snapshot.data().name,
          author: snapshot.data().author, 
          category: snapshot.data().category,
          order: snapshot.data().order
        };
        this.publicURL = this.storage.ref('gallery/' + snapshot.data().name).getDownloadURL();
      }
    )

    
    //firebase.storage().ref().child(`gallery/${this.image.name}`);
  }

}

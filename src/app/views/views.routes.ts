import { Routes, RouterModule } from '@angular/router';
import { ViewsComponent } from './views.component';
import { AdminLayoutComponent } from '../views/admin-layout/admin-layout.component';

import { UserListComponent } from './user/user-list/user-list.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { UserItemComponent } from './user/user-item/user-item.component';
import { UserUpdateComponent } from './user/user-update/user-update.component';

import { ImageListComponent } from './image/image-list/image-list.component';
import { ImageCreateComponent } from './image/image-create/image-create.component';
import { ImageItemComponent } from './image/image-item/image-item.component';
import { ImageUpdateComponent } from './image/image-update/image-update.component';


const viewsRoutes: Routes = [
  {
    path: '',
    component: ViewsComponent,
    canActivate: [],
    children: [
      { path: '', redirectTo: '/admin', pathMatch: 'full' },
      { path: 'admin', component: AdminLayoutComponent },
      { path: 'user', component: UserListComponent, data: { title: 'Usuarios' } },
      { 
        path: 'images',
        children: [
          { path: '', component: ImageListComponent },
          { path: 'new', component: ImageCreateComponent },
          { path: 'show/:id', component: ImageItemComponent },
          { path: 'edit/:id', component: ImageUpdateComponent }
        ]
      }
    ]
  }
];

export const VIEWS_ROUTES = RouterModule.forChild(viewsRoutes);

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }                  from '@angular/forms';
import { ViewsComponent } from './views.component';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { VIEWS_ROUTES } from './views.routes';
import { SharedModule } from '../shared/shared.module';

import { FileSizePipe } from '../file-size.pipe';

import { UserListComponent } from './user/user-list/user-list.component';
import { UserCreateComponent } from './user/user-create/user-create.component';
import { UserItemComponent } from './user/user-item/user-item.component';
import { UserUpdateComponent } from './user/user-update/user-update.component';

import { ImageListComponent } from './image/image-list/image-list.component';
import { ImageCreateComponent } from './image/image-create/image-create.component';
import { ImageItemComponent } from './image/image-item/image-item.component';
import { ImageUpdateComponent } from './image/image-update/image-update.component';

import { ModalModule } from 'ngx-bootstrap/modal';

@NgModule({
  declarations: [
    ViewsComponent, 
    AdminLayoutComponent,
 
    UserListComponent,
    UserCreateComponent, 
    UserItemComponent,
    UserUpdateComponent, 
    
    ImageListComponent,
    ImageCreateComponent,
    ImageItemComponent,
    ImageUpdateComponent,
    
    FileSizePipe
  ],
  imports: [
    BrowserModule, 
    VIEWS_ROUTES, 
    CommonModule, 
    SharedModule, 
    FormsModule,
    ModalModule.forRoot()
  ],
  exports: [
    AdminLayoutComponent, 
    ViewsComponent
  ]
})
export class ViewsModule {}

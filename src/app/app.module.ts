import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Modules
import { ViewsModule } from './views/views.module';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database'; // for realtime database
import { AngularFirestoreModule } from 'angularfire2/firestore'; // for cloud firestore
import { AngularFireStorageModule } from 'angularfire2/storage'; // for storage firestore
 
import { environment } from '../environments/environment';
// import { HttpModule } from '@angular/http';

// Routes
import { APP_ROUTES } from './app.routing';

// Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DropZoneDirective } from './drop-zone.directive';

@NgModule({
  declarations: [AppComponent, LoginComponent, DropZoneDirective],
  imports: [
    BrowserModule, 
    //HttpModule, 
    APP_ROUTES, 
    ViewsModule, 
    CommonModule, 
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, // for realtime database
    AngularFirestoreModule, // for cloud firestore
    AngularFireStorageModule, // for storage firestore
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ServiceModule } from '../services/service.module';
import { TitleComponent } from './title/title.component';

@NgModule({
  declarations: [HeaderComponent, SidebarComponent, TitleComponent],
  exports: [HeaderComponent, SidebarComponent, TitleComponent],
  imports: [RouterModule, CommonModule, ServiceModule]
})
export class SharedModule {}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  menu: any = [
    {
      title: 'Usuarios',
      icon: 'person_outline',
      url: '/user'
    },
    // {
    //   title: 'Mensajes',
    //   icon: 'message',
    //   url: '/messages'
    // },
    // {
    //   title: 'Productos',
    //   icon: 'store_mall_directory',
    //   url: '/products'
    // },
    {
      title: 'Imagenes',
      icon: 'photo_size_select_actual',
      url: '/images'
    }
  ];

  constructor() {}
}
